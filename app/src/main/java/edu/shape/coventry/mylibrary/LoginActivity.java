package edu.shape.coventry.mylibrary;

import androidx.appcompat.app.AppCompatActivity;
import edu.shape.coventry.mylibrary.DatabaseModule.DatabaseHelper;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    private Button btnLogin;
    private EditText edt_username, edt_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnLogin = findViewById(R.id.btnLogin);
        edt_username = findViewById(R.id.edt_username);
        edt_password = findViewById(R.id.edt_password);

        btnLogin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String usernameInput = edt_username.getText().toString();
                String passwordInput = edt_password.getText().toString();
                checkLogin(usernameInput, passwordInput);
            }
        });

    }
    public void checkLogin(String username, String password){
        DatabaseHelper db = new DatabaseHelper(this);
        String mid = db.checkMember(username, password);
        //check the login result
        if(mid.equals("no")){
            edt_username.setText("");
            edt_password.setText("");
            Toast.makeText(this, "Login Fail", Toast.LENGTH_LONG).show();
        } else {
            //display the message in toast as "Login Success".
            Toast.makeText(this,"Login Success", Toast.LENGTH_LONG);
            //move on to the book record page
            Intent i = new Intent(LoginActivity.this, BookRecordActivity.class);
            i.putExtra("mid", mid);
            startActivity(i);
        }
    }
}
