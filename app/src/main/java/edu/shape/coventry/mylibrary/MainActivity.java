package edu.shape.coventry.mylibrary;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {

    private ImageButton ibtn_search;
    private ImageButton ibtn_scan;
    private ImageButton ibtn_gps;
    private ImageButton ibtn_record;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ibtn_search = findViewById(R.id.ibtn_search);
        ibtn_scan = findViewById(R.id.ibtn_scan);
        ibtn_gps = findViewById(R.id.ibtn_gps);
        ibtn_record = findViewById(R.id.ibtn_record);

        //this listener for go to search book activity page
        ibtn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runSearchBookActivity();
            }
        });
        //this listener for go to biometric login to view record activity page
        ibtn_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runBiometricLogin();
            }
        });
        //this listener for go to GPS find library location activity page
        ibtn_gps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runGPS();
            }
        });
        //this listener for go to login to view record activity page
        ibtn_record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runRecord();
            }
        });
    }

    public void runSearchBookActivity() {
        Intent intent = new Intent(this, SearchBookActivity.class);
        startActivity(intent);
    }

    public void runBiometricLogin() {
        Intent intent = new Intent(this, BiometricLoginActivity.class);
        startActivity(intent);
    }

    public void runGPS() {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }

    public void runRecord() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

}
