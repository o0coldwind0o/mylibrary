package edu.shape.coventry.mylibrary;

import androidx.appcompat.app.AppCompatActivity;
import edu.shape.coventry.mylibrary.DatabaseModule.DatabaseHelper;

import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.biometrics.BiometricPrompt;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class BiometricLoginActivity extends AppCompatActivity {
    private Button btn_authenticate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_biometric);
        btn_authenticate = findViewById(R.id.btn_authenticate);
        final Executor executor = Executors.newSingleThreadExecutor();
        final BiometricLoginActivity biometricLoginActivity = this;
        final BiometricPrompt biometricPrompt = new BiometricPrompt.Builder(this)
                .setTitle(getString(R.string.biometric_title))
                .setSubtitle(getString(R.string.biometric_subtitle))
                .setDescription(getString(R.string.biometric_description))
                .setNegativeButton(getString(R.string.biometric_negative_button_text)
                        , executor, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                            }
                        }).build();

        btn_authenticate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                biometricPrompt.authenticate(new CancellationSignal(), executor, new BiometricPrompt.AuthenticationCallback() {
                    @Override
                    public void onAuthenticationSucceeded(BiometricPrompt.AuthenticationResult result) {
                        biometricLoginActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                DatabaseHelper db = new DatabaseHelper(BiometricLoginActivity.this);
                                String mid = db.checkMember("joe", "12345");
                                //display the message in toast as "Login Success".
                                Toast.makeText(BiometricLoginActivity.this, "Login Success", Toast.LENGTH_LONG);
                                //move on to the book record page
                                Intent i = new Intent(BiometricLoginActivity.this, BookRecordActivity.class);
                                i.putExtra("mid", mid);
                                startActivity(i);
                            }
                        });
                    }
                });
            }
        });
    }
}