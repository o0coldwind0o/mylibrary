package edu.shape.coventry.mylibrary;

import androidx.appcompat.app.AppCompatActivity;
import edu.shape.coventry.mylibrary.DatabaseModule.BookRecord;
import edu.shape.coventry.mylibrary.DatabaseModule.DatabaseHelper;

import android.os.Bundle;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class BookRecordActivity extends AppCompatActivity {
    private TextView tv_record;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_record);
        String mid = getIntent().getStringExtra("mid");
        tv_record = findViewById(R.id.tv_record);
        searchRecord(mid);

    }

    public void searchRecord(String mid) {
        List<BookRecord> record = new ArrayList<>();
        String display = "Borrow book record:\n";
        DatabaseHelper db = new DatabaseHelper(this);
        record = db.getAllRecord(mid);
        for (BookRecord bookrecord : record) {
            display += "Book Name: \n" + bookrecord.getBookname()
                    + "\nLend Date: \n" + bookrecord.getLenddate();
        }
        tv_record.setText(display);
    }
}
