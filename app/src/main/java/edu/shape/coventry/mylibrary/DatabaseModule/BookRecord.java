package edu.shape.coventry.mylibrary.DatabaseModule;

public class BookRecord {
    private String bookname, lenddate;

    public BookRecord() {
    }

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    public String getLenddate() {
        return lenddate;
    }

    public void setLenddate(String lenddate) {
        this.lenddate = lenddate;
    }
}
