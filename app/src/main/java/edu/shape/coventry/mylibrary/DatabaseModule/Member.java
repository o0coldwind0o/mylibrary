package edu.shape.coventry.mylibrary.DatabaseModule;

public class Member {
    private int mid;
    private String username, password;
    private int age;

    public Member() {
    }

    public Member(int mid, String username, String password) {
        this.mid = mid;
        this.username = username;
        this.password = password;
    }

    public Member(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public int getMid() {
        return mid;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
