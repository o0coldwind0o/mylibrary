package edu.shape.coventry.mylibrary.DatabaseModule;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
    //Database Version
    private static final int DATABASE_VERSION = 1;
    //Database Name
    private static final String DATABASE_NAME = "library";
    //Member table name
    private static final String TABLE_MEMBER = "member";
    //Member Table Columns name
    private static final String COLUMN_MID = "mid";
    private static final String COLUMN_USERNAME = "username";
    private static final String COLUMN_PASSWORD = "password";
    private static final String COLUMN_AGE = "age";
    //Book table name
    private static final String TABLE_BOOK = "book";
    //Book Table Columns name
    private static final String COLUMN_BID = "bid";
    private static final String COLUMN_BOOKNAME = "bookname";
    private static final String COLUMN_BOOKDESC = "bookdesc";
    private static final String COLUMN_NUMBER = "number";

    //Record table name
    private static final String TABLE_RECORD = "record";
    //Record Table Columns name
    private static final String COLUMN_RID = "rid";
    private static final String COLUMN_LENDDATE = "lenddate";

    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db){
        String sql;
        // Create table
        sql = "CREATE TABLE Member(" + "Mid int PRIMARY KEY ,"
                + "Username text NOT NULL, " + "Password text NOT NULL, " + "Age int NOT NULL); ";
        db.execSQL(sql);
        sql = "CREATE TABLE Book(" + "Bid int PRIMARY KEY ,"
                + "Bookname text, " + "Bookdesc text, " + "number int); ";
        db.execSQL(sql);
        sql = "CREATE TABLE Record(" + "Rid int PRIMARY KEY ,"
                + "mid int, " + "bid int, " + "lenddate datetime NOT NULL DEFAULT CURRENT_TIMESTAMP, " +
                " CONSTRAINT fk_book_record FOREIGN KEY (bid) REFERENCES Book (bid), " +
                " CONSTRAINT fk_member_record FOREIGN KEY (mid) REFERENCES Member (mid)); ";
        db.execSQL(sql);
        // Add member rows
        db.execSQL("INSERT INTO Member(Mid, Username, Password, Age) values"
                + "(1001, 'joe', '12345', 19); ");
        db.execSQL("INSERT INTO Member(Mid, Username, Password, Age) values"
                + "(1002, 'HelenLeung', '88888', 25); ");
        db.execSQL("INSERT INTO Member(Mid, Username, Password, Age) values"
                + "(1003, 'RobertChan', 'iloveu', 61); ");
        db.execSQL("INSERT INTO Member(Mid, Username, Password, Age) values"
                + "(1004, 'CarolWong', 'peterpan', 33); ");
        db.execSQL("INSERT INTO Member(Mid, Username, Password, Age) values"
                + "(1005, 'CarmanWong', 'pooh', 44); ");
        db.execSQL("INSERT INTO Member(Mid, Username, Password, Age) values"
                + "(1006, 'JohnChan', 'johnchan', 28); ");
        db.execSQL("INSERT INTO Member(Mid, Username, Password, Age) values"
                + "(1007, 'PaulLam', 'apple', 16); ");

        // Add book rows
        db.execSQL("INSERT INTO Book(bid, bookname, bookdesc, number) values"
                + "(1, 'Coronavirus: A Book for Children', " +
                "'What is the coronavirus, and why is everyone talking about it? " +
                "Engagingly illustrated by Axel Scheffler, " +
                "this approachable and timely book helps answer these questions and many more, " +
                "providing children aged 5-10 and their parents with clear and accessible " +
                "explanations about the coronavirus and its effects - " +
                "both from a health perspective and the impact it has on a " +
                "family’s day-to-day life. With input from expert consultant " +
                "Professor Graham Medley of the London School of Hygiene & Tropical Medicine, " +
                "as well as advice from teachers and child psychologists, " +
                "this is a practical and informative resource to help " +
                "explain the changes we are currently all experiencing. " +
                "The book is free to read and download, but " +
                "Nosy Crow would like to encourage readers, should they feel in a position to, " +
                "to make a donation to: https://www.nhscharitiestogether.co.uk/', 2); ");
        db.execSQL("INSERT INTO Book(bid, bookname, bookdesc, number) values"
                + "(2, 'The Monster at Our Door', 'A sobering forecast of a potentially lethal virus known as H5N1, currently affecting the poultry and wild bird populations of East Asia, evaluates the World Health Organization concerns that the virus is on the brink of mutating into a pandemic illness and cites the ecological and political factors that are contributing to the threat. Reprint.', 2); ");
        db.execSQL("INSERT INTO Book(bid, bookname, bookdesc, number) values"
                + "(3, 'Covid - 19: Coronavirus Perfect weapon or just coincidence', 'Wuhan China Coronavirus Book: A-Z knowledge - symptom - guide - prevention: Is it Perfect weapon or just coincidence? By the time this book was published, the number of 2019-nCoV strains worldwide was officially confirmed to be 67,104 and had been increasing every hour. Of course with the current situation, everyone worry about the disease is indisputable. A lot of false and inaccurate information has been spread throughout social networks, making the reader confused. This book will provide from A - Z all information about the Corona pandemic, such as: What is Coronavirus? Are all strains of Corona the same? How to know the exact information about the disease? What is the most effective way of preventing this pandemic? How to use the face mask hand sanitizer gloves Lysol respirator? Infrared precautions thermometer incubation period; in cats what is coronavirus human feline zombies? Symptoms in babies canine sars mers vaccine; Vaccination definition in horses treatment; Infection middle east respiratory syndrome; How to protect yourself? The truth behind the rumors? How to prevent and overcome pandemics? #Coronavirus #Covid19 #chinesevirus #wuhanvirus #whocorona #coronatreatment #coronavirusindia #coronavirusamerica #coronavirusitely', 0); ");
        db.execSQL("INSERT INTO Book(bid, bookname, bookdesc, number) values"
                + "(4, 'Pale Rider', 'In 1918, the Italian-Americans of New York, the Yupik of Alaska and the Persians of Mashed had almost nothing in common except for a virus--one that triggered the worst pandemic of modern times and had a decisive effect on the history of the twentieth century. The Spanish flu of 1918-1920 was one of the greatest human disasters of all time. It infected a third of the people on Earth--from the poorest immigrants of New York City to the king of Spain, Franz Kafka, Mahatma Gandhi and Woodrow Wilson. But despite a death toll of between 50 and 100 million people, it exists in our memory as an afterthought to World War I. In this gripping narrative history, Laura Spinney traces the overlooked pandemic to reveal how the virus travelled across the globe, exposing mankind vulnerability and putting our ingenuity to the test. As socially significant as both world wars, the Spanish flu dramatically disrupted--and often permanently altered--global politics, race relations and family structures, while spurring innovation in medicine, religion and the arts. It was partly responsible, Spinney argues, for pushing India to independence, South Africa to apartheid and Switzerland to the brink of civil war. It also created the true \\\"lost generation.\\\" Drawing on the latest research in history, virology, epidemiology, psychology and economics, Pale Rider masterfully recounts the little-known catastrophe that forever changed humanity.', 2); ");

        // Add member rows
        db.execSQL("INSERT INTO Record(rid, mid, bid, lenddate) values"
                + "(1, 1001, 3, '2020-04-26 18:32:54'); ");
        db.execSQL("INSERT INTO Record(rid, mid, bid, lenddate) values"
                + "(2, 1004, 3, '2020-04-26 18:33:21'); ");
    }

    //Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int odlVersion, int newVersion){
        //Drop older table if existed
        db.execSQL("DROP TABLE if exists Member;");
        db.execSQL("DROP TABLE if exists Book;");
        db.execSQL("DROP TABLE if exists Record;");
        //Create tables again
        onCreate(db);
    }

    public String checkMember(String usernameInput, String passwordInput){
        String mid = "no";
        String loginQuery  = "SELECT " + COLUMN_MID + " FROM " + TABLE_MEMBER + " WHERE " + COLUMN_USERNAME + " = '" +
                usernameInput + "' AND " + COLUMN_PASSWORD + " = '" + passwordInput + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(loginQuery, null);
        //looping
        if(cursor.moveToNext()) {
            mid = cursor.getString(0);
        }
        db.close();
        return mid;
    }

    public List<Book> searchBook(String keyword){
        List<Book> bookList = new ArrayList<Book>();
        //Select All Query about the keyword
        String searchQuery = "SELECT * FROM " + TABLE_BOOK + " WHERE " + COLUMN_BOOKNAME + " LIKE '%" + keyword
                + "%' OR " + COLUMN_BOOKDESC + " LIKE '%" + keyword + "%'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(searchQuery, null);
        //looping
        if(cursor.moveToFirst()) {
            do{
                Book book = new Book();
                book.setBid(cursor.getString(0));
                book.setBookname(cursor.getString(1));
                book.setBookdesc(cursor.getString(2));
                book.setNumber(Integer.parseInt(cursor.getString(3)));
                //Adding book to list
                bookList.add(book);
            } while (cursor.moveToNext());
        }
        db.close();
        return bookList;

    }

    public List<Book> getAllBook(){
        List<Book> bookList = new ArrayList<Book>();
        //Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_BOOK;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        //looping
        if(cursor.moveToFirst()) {
            do{
                Book book = new Book();
                book.setBid(cursor.getString(0));
                book.setBookname(cursor.getString(1));
                book.setBookdesc(cursor.getString(2));
                book.setNumber(Integer.parseInt(cursor.getString(3)));
                //Adding book to list
                bookList.add(book);
            } while (cursor.moveToNext());
        }
        db.close();
        return bookList;
    }

    public List<BookRecord> getAllRecord(String mid){
        List<BookRecord> bookRecordList = new ArrayList<BookRecord>();
        //Select All Query
        String selectRecordQuery = "SELECT " + "book.bookname, lenddate FROM " + TABLE_RECORD + ", " + TABLE_BOOK + ", "
                + TABLE_MEMBER + " WHERE " + TABLE_RECORD + "." + COLUMN_MID + " = " + mid +
                " AND member.mid = record.mid " + " AND book.bid = record.bid";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectRecordQuery, null);

        //looping
        if(cursor.moveToFirst()) {
            do{
                BookRecord bookRecord = new BookRecord();
                bookRecord.setBookname(cursor.getString(0));
                bookRecord.setLenddate(cursor.getString(1));

                //Adding book to list
                bookRecordList.add(bookRecord);
            } while (cursor.moveToNext());
        }
        db.close();
        return bookRecordList;
    }

    public String checkMembor(String usernameInput, String passwordInput){
        String mid = "1001";
        return mid;
    }
}
