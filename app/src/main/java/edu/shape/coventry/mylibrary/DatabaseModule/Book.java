package edu.shape.coventry.mylibrary.DatabaseModule;

public class Book {
    private String bid, bookname, bookdesc;
    private int number;

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    public String getBookdesc() {
        return bookdesc;
    }

    public void setBookdesc(String bookdesc) {
        this.bookdesc = bookdesc;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Book() {
    }

    public Book(String bid, String bookname, String bookdesc, int number) {
        this.bid = bid;
        this.bookname = bookname;
        this.bookdesc = bookdesc;
        this.number = number;
    }
}
