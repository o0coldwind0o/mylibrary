package edu.shape.coventry.mylibrary;

import androidx.appcompat.app.AppCompatActivity;
import edu.shape.coventry.mylibrary.DatabaseModule.Book;
import edu.shape.coventry.mylibrary.DatabaseModule.DatabaseHelper;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class SearchBookActivity extends AppCompatActivity {
    private TextView tv_data;
    private Button btn_searchAll, btn_searchKey;
    private EditText edt_keyword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_book);
        //DatabaseHelper dbHelper = new DatabaseHelper(this);
        edt_keyword = findViewById(R.id.edt_keyword);
        tv_data = findViewById(R.id.tv_data);
        tv_data.setMovementMethod(ScrollingMovementMethod.getInstance());
        btn_searchAll = findViewById(R.id.btn_searchAll);
        btn_searchKey = findViewById(R.id.btn_searchKey);


        btn_searchAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btn_searchAll.isClickable()) {
                    searchALLBook();
                }
            }
        });

        btn_searchKey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btn_searchKey.isClickable()) {
                    String keyword = edt_keyword.getText().toString();
                    if(keyword.equals("")){
                        tv_data.setText("Please input the keyword about book name or description to search book");
                    } else {
                        searchKeyword(keyword);
                    }
                }
            }
        });
    }

    public void searchKeyword(String keyword){
        List<Book> books = new ArrayList<>();
        String display = "";
        DatabaseHelper db = new DatabaseHelper(this);
        books = db.searchBook(keyword);
        for(Book book:books){
            display += "BID: " + book.getBid() + "\nBook Name: " + book.getBookname()
                    + "\nBook Description: \n" + book.getBookdesc() + "\nNumber of books: " + book.getNumber()
                    + "\n\n";
        }
        tv_data.setText(display);
    }

    public void searchALLBook(){
        List<Book> books = new ArrayList<>();
        String display = "";
        DatabaseHelper db = new DatabaseHelper(this);
        books = db.getAllBook();
        for(Book book:books){
            display += "BID: " + book.getBid() + "\nBook Name: " + book.getBookname()
                    + "\nBook Description: \n" + book.getBookdesc() + "\nNumber of books: " + book.getNumber()
                    + "\n\n";
        }
        tv_data.setText(display);
    }
}
