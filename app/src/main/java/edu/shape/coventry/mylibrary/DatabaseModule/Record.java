package edu.shape.coventry.mylibrary.DatabaseModule;

public class Record {
    private int rid, mid, bid;
    private String lenddate;

    public Record(int mid, int bid, String lenddate) {
        this.mid = mid;
        this.bid = bid;
        this.lenddate = lenddate;
    }

    public Record() {

    }

    public int getRid() {
        return rid;
    }

    public void setRid(int rid) {
        this.rid = rid;
    }

    public int getMid() {
        return mid;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    public int getBid() {
        return bid;
    }

    public void setBid(int bid) {
        this.bid = bid;
    }

    public String getLenddate() {
        return lenddate;
    }

    public void setLenddate(String lenddate) {
        this.lenddate = lenddate;
    }
}
